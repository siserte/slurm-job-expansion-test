#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <unistd.h>
#include "slurm/slurm.h"
#include "slurm/slurm_errno.h"

void getHostlist(char * nodelist, char * hostlist) {
    hostlist_t hl = slurm_hostlist_create(nodelist);
    char *host = slurm_hostlist_shift(hl);
    while (host != NULL) {
        sprintf(hostlist, "%s,%s", hostlist, host);
        host = slurm_hostlist_shift(hl);
    }
    slurm_hostlist_destroy(hl);
}

int main(int argc, char** argv) {
    int rank, world_size, name_len, hostsToExpand;
    char processor_name[MPI_MAX_PROCESSOR_NAME];

    MPI_Init(NULL, NULL);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Get_processor_name(processor_name, &name_len);

    MPI_Comm parent;
    MPI_Comm_get_parent(&parent);

    MPI_Info info;
    MPI_Info_create(&info);

    if (parent == MPI_COMM_NULL) {
        hostsToExpand = world_size * 2;
        if (rank == 0) {
            printf("[%d/%d] Starting resize... (%s).\n", rank, world_size, processor_name);

            char hostlist[256] = "";
            char *pID = getenv("SLURM_JOBID");
            uint32_t procID = atoi(pID);
            job_info_msg_t *MasterInfo;
            job_desc_msg_t job, job_update;
            resource_allocation_response_msg_t* slurm_alloc_msg_ptr;

            slurm_load_job(&MasterInfo, procID, 0);
            if (MasterInfo == NULL || MasterInfo->record_count == 0) {
                slurm_perror((char *) "No info");
                exit(1);
            }
            getHostlist(MasterInfo->job_array->nodes, hostlist);
            printf("[%d/%d] Master hostlist %s\n", rank, world_size, hostlist);

            partition_info_msg_t *new_part_ptr = NULL;
            slurm_load_partitions((time_t) NULL, &new_part_ptr, (uint16_t) 0);
            partition_info_t part;
            part = new_part_ptr->partition_array[0];
            //slurm_sprint_partition_info(&part, 0);
            printf("[%d/%d] Queue info: Nodes: %d\n", rank, world_size, part.total_nodes);
            slurm_free_partition_info_msg(new_part_ptr);

            slurm_init_job_desc_msg(&job);
            job.user_id = getuid();
            job.min_nodes = hostsToExpand - MasterInfo->job_array->num_nodes;
            job.dependency = (char *) malloc(sizeof (char)*20);
            sprintf(job.dependency, (char *) "expand:%s", pID);
            //$ salloc -N4 --dependency=expand:$SLURM_JOBID

            //slurm_alloc_msg_ptr = slurm_allocate_resources_blocking(&job, 0, NULL);
            if (slurm_allocate_resources(&job, &slurm_alloc_msg_ptr) != 0) {
                slurm_perror("allocation error");
                exit(1);
            }

            if (slurm_alloc_msg_ptr->node_list == NULL) {
                slurm_perror("The job cannot be executed immediately");
                exit(1);
            }
            printf("(debug): %s(%s,%d) - %d %s\n", __FILE__, __func__, __LINE__, slurm_alloc_msg_ptr->job_id, slurm_alloc_msg_ptr->node_list);
            //$ scontrol update jobid=$SLURM_JOBID NumNodes=0
            slurm_init_job_desc_msg(&job_update);
            job_update.job_id = slurm_alloc_msg_ptr->job_id;
            job_update.min_nodes = 0;
            slurm_update_job(&job_update);
            //exit
            slurm_kill_job(slurm_alloc_msg_ptr->job_id, 9, 0);
            //$ scontrol update jobid=$SLURM_JOBID NumNodes=ALL
            slurm_init_job_desc_msg(&job_update);
            job_update.job_id = procID;
            job_update.min_nodes = INFINITE;
            slurm_update_job(&job_update);

            getHostlist(slurm_alloc_msg_ptr->node_list, hostlist);
            MPI_Info_set(info, "hosts", hostlist);
            printf("[%d/%d] Spawning in %s (%d)\n", rank, world_size, hostlist, hostsToExpand);
        }
        MPI_Comm_spawn("./jobExpansion", MPI_ARGV_NULL, hostsToExpand, info, 0, MPI_COMM_WORLD, &parent, MPI_ERRCODES_IGNORE);
    } else {
        sleep(2);
        printf("[%d/%d] Hello from %d of %d ranks (%s).\n", rank, world_size, rank, world_size, processor_name);
    }
    MPI_Finalize();
    return 0;
}
