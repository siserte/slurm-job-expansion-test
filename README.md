# README #


### What is this repository for? ###

This repository is aim at testing the Slurm RMS job expansion feature using the Slurm API.
For testing purpose, this version only expands 2x the initial processes, always in different nodes.


### How do I get set up? ###

* Configuration
Compile with:
  mpicc -I/path/to/slumr/slurm/include -L/path/to/slurm/lib -lslurm -I/path/to/mpi/include -L/path/to/mpi/lib -lmpi -Wall testSlurmExpansion.c -o testSlurmExpansion


* Dependencies
MPI library
Slurm Resource Manager


* How to run tests
Firstly export the environmental variable (if needed) of MPI and Slurm:
  export LD_LIBRARY_PATH=/path/to/slurm/lib:/path/to/mpi/lib/:$LD_LIBRARY_PATH

Then, we allocate resources:
  salloc -N1

And execute the application, giving the allocated nodes:
  mpiexec -n $SLURM_JOB_NUM_NODES -hosts $SLURM_NODELIST ./testSlurmExpansion


### Who do I talk to? ###

Sergio Iserte
siserte@uji.es
